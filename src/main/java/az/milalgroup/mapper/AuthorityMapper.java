package az.milalgroup.mapper;

import az.milalgroup.domain.Authority;
import az.milalgroup.dto.response.AuthorityResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AuthorityMapper {

    AuthorityResponse mapToResponse(Authority authority);

}
