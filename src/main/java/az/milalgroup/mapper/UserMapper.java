package az.milalgroup.mapper;

import az.milalgroup.domain.User;
import az.milalgroup.dto.request.UserRegisterRequest;
import az.milalgroup.dto.response.UserResponse;
import az.milalgroup.enumaration.UserStatus;
import az.milalgroup.mapper.qualifier.UserQualifier;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring",
        uses = {UserQualifier.class, AuthorityMapper.class},
        imports = {UserStatus.class, Boolean.class}
)
public interface UserMapper {

    @Mapping(source = "password", target = "password", qualifiedByName = "encodePassword")
    User mapToEntity(UserRegisterRequest request);

    UserResponse mapToResponse(User user);

}
