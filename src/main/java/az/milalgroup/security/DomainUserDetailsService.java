package az.milalgroup.security;

import az.milalgroup.domain.Authority;
import az.milalgroup.domain.User;
import az.milalgroup.enumaration.UserStatus;
import az.milalgroup.exception.AuthUsernameNotFoundException;
import az.milalgroup.exception.UserBlockedException;
import az.milalgroup.exception.UserNotConfirmedException;
import az.milalgroup.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

import static az.milalgroup.enumaration.UserStatus.BLOCKED;
import static az.milalgroup.enumaration.UserStatus.PENDING;

@Service
@RequiredArgsConstructor
public class DomainUserDetailsService implements UserDetailsService {

    private final UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return repository.findOneWithAuthoritiesByUsername(username)
                .map(this::createSpringSecurityUser)
                .orElseThrow(() -> new AuthUsernameNotFoundException(new Object[]{username}));
    }

    private org.springframework.security.core.userdetails.User createSpringSecurityUser(User user) {
        checkUserConfirmationStatus(user.getUsername(), user.getUserStatus());
        List<SimpleGrantedAuthority> grantedAuthorities = user
                .getAuthorities()
                .stream()
                .map(Authority::getName)
                .map(SimpleGrantedAuthority::new)
                .toList();
        return new org.springframework.security.core.userdetails.User(
                user.getUsername(), user.getPassword(), grantedAuthorities);
    }

    private void checkUserConfirmationStatus(String username, UserStatus userStatus) {
        if (Objects.equals(userStatus, PENDING)) {
            throw new UserNotConfirmedException(new Object[]{username});
        }
        if (Objects.equals(userStatus, BLOCKED)) {
            throw new UserBlockedException(new Object[]{username});
        }
    }

}
