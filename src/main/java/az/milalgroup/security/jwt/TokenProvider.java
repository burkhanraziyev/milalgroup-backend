package az.milalgroup.security.jwt;

import az.milalgroup.config.ApplicationProperties;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.stream.Collectors;

import static az.milalgroup.constant.AuthoritiesConstants.AUTHORITIES_KEY;

@Slf4j
@Component
public class TokenProvider {

    private final ApplicationProperties applicationProperties;
    private final Key key;

    public TokenProvider(ApplicationProperties applicationProperties) {
        final var secretKey = applicationProperties.getSecurityProperties().getJwtProperties().getSecret();
        this.applicationProperties = applicationProperties;
        this.key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(secretKey));
    }

    public String createToken(Authentication authentication, boolean rememberMe) {
        return Jwts.builder()
                .setSubject(authentication.getName())
                .claim(AUTHORITIES_KEY, parseAuthorities(authentication))
                .signWith(key, SignatureAlgorithm.HS512)
                .setExpiration(Date.from(getExpirationDate(rememberMe)))
                .compact();
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token);
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            log.trace("Invalid JWT token trace.", e);
        }
        return false;
    }

    public String getSubjectFromToken(String token) {
        Jws<Claims> claimsJws = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token);
        return claimsJws.getBody().getSubject();
    }

    private Instant getExpirationDate(boolean rememberMe) {
        return rememberMe
                ?
                ZonedDateTime.now()
                        .plusDays(applicationProperties
                                .getSecurityProperties()
                                .getJwtProperties()
                                .getTokenValidityForRememberMe())
                        .toInstant() :
                ZonedDateTime.now()
                        .plusDays(applicationProperties
                                .getSecurityProperties()
                                .getJwtProperties()
                                .getTokenValidity())
                        .toInstant();

    }

    private String parseAuthorities(Authentication authentication) {
        return authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","));
    }

}
