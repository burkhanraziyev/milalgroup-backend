package az.milalgroup.dto.response;

import az.milalgroup.enumaration.EventRequestStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EventRequestStatusResponse {

    private Integer id;
    private String value;
    private String name;

    public EventRequestStatusResponse(EventRequestStatus eventRequestStatus) {
        this.id = eventRequestStatus.getId();
        this.value = eventRequestStatus.getValue();
        this.name = eventRequestStatus.name();
    }

}
