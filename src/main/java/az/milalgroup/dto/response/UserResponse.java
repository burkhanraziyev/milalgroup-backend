package az.milalgroup.dto.response;

import az.milalgroup.dto.GenderEnumResponse;
import az.milalgroup.dto.UserStatusEnumResponse;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserResponse {

    private Long id;
    private String firstname;
    private String lastname;
    private String username;
    @JsonFormat(pattern = "dd-MM-yyyy", shape = JsonFormat.Shape.STRING)
    private LocalDateTime birthOfDate;
    private GenderEnumResponse genderEnumResponse;
    private UserStatusEnumResponse userStatus;
    private Set<AuthorityResponse> authorities;
    private Boolean isEmailConfirmed;
    private Boolean isPhoneNumberConfirmed;

}
