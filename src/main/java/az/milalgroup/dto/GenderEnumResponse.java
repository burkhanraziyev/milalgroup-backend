package az.milalgroup.dto;

import az.milalgroup.enumaration.Gender;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenderEnumResponse {

    private Integer id;
    private String value;
    private String name;

    public GenderEnumResponse(Gender gender) {
        this.id = gender.getId();
        this.value = gender.getValue();
        this.name = gender.name();
    }

}
