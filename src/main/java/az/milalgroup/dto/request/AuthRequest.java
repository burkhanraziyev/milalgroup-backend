package az.milalgroup.dto.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class AuthRequest {

    @NotBlank(message = "exception.validation.username.notBlank")
    private String username;

    @NotBlank(message = "exception.validation.password.notBlank")
    private String password;

    private boolean isRememberMe;

}
