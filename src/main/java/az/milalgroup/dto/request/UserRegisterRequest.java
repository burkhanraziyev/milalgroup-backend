package az.milalgroup.dto.request;

import az.milalgroup.enumaration.Gender;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UserRegisterRequest {

    @NotBlank(message = "exception.validation.firstname.notBlank")
    @Size(min = 3, max = 20, message = "exception.validation.firstname.size")
    private String firstname;

    @NotBlank(message = "exception.validation.lastname.notBlank")
    @Size(min = 3, max = 20, message = "exception.validation.firstname.size")
    private String lastname;

    @NotBlank(message = "exception.validation.username.notBlank")
    @Size(min = 3, max = 20, message = "exception.validation.firstname.size")
    // TODO: check unique by username
    private String username;

    @NotBlank(message = "exception.validation.email.notBlank")
    @Email(regexp = "", message = "exception.validation.email.regexp")
    // TODO: check unique by email
    private String email;

    @NotBlank(message = "exception.validation.phone.notBlank")
    // TODO: pattern validate to phone
    // TODO: check unique by phone
    private String phone;

    @NotBlank(message = "exception.validation.password.notBlank")
    private String password;

    @NotNull(message = "exception.validation.birthOfDate.notNull")
    @JsonFormat(pattern = "dd-MM-yyyy", shape = JsonFormat.Shape.STRING)
    private LocalDateTime birthOfDate;

    @NotNull(message = "exception.validation.gender.notNull")
    private Gender gender;

}