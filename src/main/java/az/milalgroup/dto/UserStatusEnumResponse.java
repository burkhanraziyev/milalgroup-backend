package az.milalgroup.dto;

import az.milalgroup.enumaration.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserStatusEnumResponse {

    private Integer id;
    private String value;
    private String name;

    public UserStatusEnumResponse(UserStatus userStatus) {
        this.id = userStatus.getId();
        this.value = userStatus.getValue();
        this.name = userStatus.name();
    }

}
