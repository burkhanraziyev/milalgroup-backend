package az.milalgroup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MilalgroupBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(MilalgroupBackendApplication.class, args);
    }

}
