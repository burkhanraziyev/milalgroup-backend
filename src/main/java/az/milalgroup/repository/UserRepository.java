package az.milalgroup.repository;

import az.milalgroup.domain.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {


    Optional<User> findByUsername(String username);

    @EntityGraph(attributePaths = {"authorities"})
    Optional<User> findOneWithAuthoritiesByUsername(String username);

}