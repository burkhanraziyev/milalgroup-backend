package az.milalgroup.repository;

import az.milalgroup.domain.EventRequest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventRequestRepository extends JpaRepository<EventRequest, Long> {
}