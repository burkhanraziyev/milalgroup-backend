package az.milalgroup.constant;

public interface ExceptionMessageKeyConstants {

    String USER_NOT_FOUND_BY_USERNAME = "exception.userNotFoundByUsername";
    String ROLE_NOT_FOUND_BY_NAME = "exception.roleNotFoundByName";
    String ENUM_MISMATCH = "exception.enumMismatch";
    String USER_BLOCKED = "exception.userBlocked";
    String USER_LOGIN_NOT_FOUND = "exception.userLoginNotFound";
    String USER_NOT_CONFIRMED = "exception.userNotConfirmed";
    String EMAIL_SEND_FAILED = "exception.emailSendFailed";
    String VALIDATION_ERROR = "exception.validation.title";

}
