package az.milalgroup.constant;

public interface AuthoritiesConstants {

    String AUTHORITIES_KEY = "auth";
    String ADMIN = "ROLE_ADMIN";
    String USER = "ROLE_USER";
    String AUTHORIZATION_HEADER = "Authorization";
    String ANONYMOUS = "anonymous";
}
