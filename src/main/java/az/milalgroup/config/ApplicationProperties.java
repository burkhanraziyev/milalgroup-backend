package az.milalgroup.config;

import jakarta.annotation.PostConstruct;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;

@Slf4j
@Data
@Configuration
@ConfigurationProperties(prefix = "application")
public class ApplicationProperties {

    @NestedConfigurationProperty
    private SecurityProperties securityProperties;

    @Data
    public static class SecurityProperties {

        @NestedConfigurationProperty
        private JwtProperties jwtProperties;
        @NestedConfigurationProperty
        private CorsConfiguration cors;

        @Data
        public static class JwtProperties {

            private String secret;
            private Long tokenValidity;
            private Long tokenValidityForRememberMe;
            private Long refreshTokenValidity;

        }
    }

    @PostConstruct
    public void log() {
        log.info("SecurityProperties: {}", securityProperties);
    }
}
