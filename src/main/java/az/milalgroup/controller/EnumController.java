package az.milalgroup.controller;

import az.milalgroup.dto.GenderEnumResponse;
import az.milalgroup.dto.UserStatusEnumResponse;
import az.milalgroup.dto.response.EventRequestStatusResponse;
import az.milalgroup.enumaration.EventRequestStatus;
import az.milalgroup.enumaration.Gender;
import az.milalgroup.enumaration.UserStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/enums")
public class EnumController {

    @GetMapping("/user-statuses")
    public ResponseEntity<List<UserStatusEnumResponse>> getAllUserStatuses() {
        final var responseList = Arrays.stream(UserStatus.values())
                .map(UserStatusEnumResponse::new)
                .toList();
        return ResponseEntity.ok(responseList);
    }

    @GetMapping("/genders")
    public ResponseEntity<List<GenderEnumResponse>> getAllGenders() {
        final var responseList = Arrays.stream(Gender.values())
                .map(GenderEnumResponse::new)
                .toList();
        return ResponseEntity.ok(responseList);
    }

    @GetMapping("/event-request-statuses")
    public ResponseEntity<List<EventRequestStatusResponse>> getAllEventRequestStatuses() {
        final var responseList = Arrays.stream(EventRequestStatus.values())
                .map(EventRequestStatusResponse::new)
                .toList();
        return ResponseEntity.ok(responseList);
    }

}
