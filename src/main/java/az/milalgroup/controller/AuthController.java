package az.milalgroup.controller;

import az.milalgroup.dto.request.AuthRequest;
import az.milalgroup.dto.response.AuthenticateResponse;
import az.milalgroup.service.AuthenticateService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class AuthController {

    private final AuthenticateService authenticateService;

    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticateResponse> authorize(@Valid @RequestBody AuthRequest authRequest) {
        final var authenticateResponse = authenticateService.authorize(authRequest);
        return new ResponseEntity<>(authenticateResponse, getAuthHeaders(authenticateResponse.getAccessToken()), OK);
    }

    // TODO: forgot password
    // TODO: refresh token
    // TODO: otp confirmation
    // TODO: email confirmation

    private HttpHeaders getAuthHeaders(String jwt) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(jwt);
        return httpHeaders;
    }

}
