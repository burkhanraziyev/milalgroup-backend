package az.milalgroup.controller;

import az.milalgroup.dto.request.UserRegisterRequest;
import az.milalgroup.dto.response.UserResponse;
import az.milalgroup.security.SecurityUtils;
import az.milalgroup.service.UserRegistrationService;
import az.milalgroup.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/profile")
public class ProfileController {

    private final UserService service;
    private final UserRegistrationService userRegistrationService;

    @PostMapping
    public ResponseEntity<UserResponse> register(@Valid @RequestBody UserRegisterRequest request) {
        final var response = userRegistrationService.publicUserRegister(request);
        final var location = ServletUriComponentsBuilder.fromCurrentRequest()
                .replacePath("/api/users/{id}")
                .buildAndExpand(response.getId())
                .toUri();
        return ResponseEntity.created(location).body(response);
    }

    @GetMapping("/me")
    public ResponseEntity<UserResponse> me() {
        final var username = SecurityUtils.getCurrentUserLoginOrThrow();
        return ResponseEntity.ok(service.getByUsername(username));
    }

    // TODO: change password
    // TODO: update profile image
    // TODO: update profile information

}
