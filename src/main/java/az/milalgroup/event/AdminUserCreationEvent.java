package az.milalgroup.event;

import org.springframework.context.ApplicationEvent;

public class AdminUserCreationEvent extends ApplicationEvent {

    public AdminUserCreationEvent(Object source) {
        super(source);
    }
}
