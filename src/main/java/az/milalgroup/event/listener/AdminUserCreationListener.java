package az.milalgroup.event.listener;

import az.milalgroup.domain.User;
import az.milalgroup.enumaration.Gender;
import az.milalgroup.enumaration.UserStatus;
import az.milalgroup.event.AdminUserCreationEvent;
import az.milalgroup.repository.UserRepository;
import az.milalgroup.security.SecurityUtils;
import az.milalgroup.service.AuthorityService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static az.milalgroup.constant.AuthoritiesConstants.ANONYMOUS;

@Component
@RequiredArgsConstructor
public class AdminUserCreationListener {

    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;
    private final AuthorityService authorityService;

    @EventListener
    public void handleAdminUserCreationEvent(AdminUserCreationEvent event) {
        final var user = User.builder()
                .username("burhan")
                .password(passwordEncoder.encode("Burh@n"))
                .firstname("Burhan")
                .lastname("Raziyev")
                .email("burhanreziyev@gmail.com")
                .phoneNumber("+994772772932")
                .birthOfDate(getFormattedDate(LocalDateTime.of(2002, 1, 21, 0, 0)))
                .createdDate(getFormattedDate(LocalDateTime.now()))
                .lastModifiedDate(getFormattedDate(LocalDateTime.now()))
                .createdBy(SecurityUtils.getCurrentUserLogin().orElse(ANONYMOUS))
                .lastModifiedBy(SecurityUtils.getCurrentUserLogin().orElse(ANONYMOUS))
                .gender(Gender.MALE)
                .userStatus(UserStatus.ACTIVE)
                .authorities(authorityService.getAdminRoles())
                .build();
        repository.save(user);
    }

    private LocalDateTime getFormattedDate(LocalDateTime localDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        final var formattedDateTime = localDateTime.format(formatter);
        return LocalDateTime.parse(formattedDateTime, formatter);
    }
}
