package az.milalgroup.service;

import az.milalgroup.constant.AuthoritiesConstants;
import az.milalgroup.domain.Authority;
import az.milalgroup.exception.DomainNotFoundException;
import az.milalgroup.repository.AuthorityRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static az.milalgroup.constant.ExceptionMessageKeyConstants.ROLE_NOT_FOUND_BY_NAME;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class AuthorityService {

    private final AuthorityRepository repository;

    @Transactional(readOnly = true)
    public Set<Authority> getDefaultUserRoles() {
        return Set.of(findById(AuthoritiesConstants.USER));
    }

    public Set<Authority> getAdminRoles() {
        return Set.of(findById(AuthoritiesConstants.ADMIN));
    }

    @Transactional(readOnly = true)
    public Authority findById(String roleName) {
        return repository.findById(roleName)
                .orElseThrow(() ->
                        new DomainNotFoundException(ROLE_NOT_FOUND_BY_NAME, new Object[]{roleName}));
    }

}
