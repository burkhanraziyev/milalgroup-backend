package az.milalgroup.service;

import az.milalgroup.domain.User;
import az.milalgroup.dto.response.UserResponse;
import az.milalgroup.exception.DomainNotFoundException;
import az.milalgroup.mapper.UserMapper;
import az.milalgroup.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static az.milalgroup.constant.ExceptionMessageKeyConstants.USER_NOT_FOUND_BY_USERNAME;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class UserService {

    private final UserRepository repository;
    private final UserMapper mapper;

    @Transactional(readOnly = true)
    public UserResponse getByUsername(String username) {
        return mapper.mapToResponse(findOneByUsernameWithAuthorities(username));
    }

    @Transactional(readOnly = true)
    public User findOneByUsernameWithAuthorities(String username) {
        return repository.findOneWithAuthoritiesByUsername(username)
                .orElseThrow(() -> new DomainNotFoundException(USER_NOT_FOUND_BY_USERNAME, new Object[]{username}));
    }

    @Transactional(readOnly = true)
    public User findByUsername(String username) {
        return repository.findByUsername(username)
                .orElseThrow(() -> new DomainNotFoundException(USER_NOT_FOUND_BY_USERNAME, new Object[]{username}));
    }

}
