package az.milalgroup.service;

import az.milalgroup.dto.request.UserRegisterRequest;
import az.milalgroup.dto.response.UserResponse;
import az.milalgroup.mapper.UserMapper;
import az.milalgroup.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class UserRegistrationService {

    private final UserMapper mapper;
    private final AuthorityService authorityService;
    private final UserRepository repository;

    public UserResponse publicUserRegister(UserRegisterRequest request) {
        var user = mapper.mapToEntity(request);
        user.setAuthorities(authorityService.getDefaultUserRoles());
        user = repository.saveAndFlush(user);
        return mapper.mapToResponse(user);
    }

}
