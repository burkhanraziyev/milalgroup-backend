package az.milalgroup.service;

import az.milalgroup.domain.User;
import az.milalgroup.exception.EmailSendFailedException;
import jakarta.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmailConfirmationService {

    private final EmailVerificationTokenService emailVerificationTokenService;
    private final TemplateEngine templateEngine;
    private final MailService mailService;

    public void sendConfirmationEmailToUser(User user) {
        final var token = UUID.randomUUID().toString();
        emailVerificationTokenService.create(user, token);
        Context context = new Context();
        context.setVariable("confirmationLink", buildConfirmationLink(token));

        String htmlBody = templateEngine.process("email_confirmation_template", context);

        try {
            mailService.sendEmail(user.getEmail(), "E-poçt Doğrulaması", htmlBody);
        } catch (MessagingException e) {
            throw new EmailSendFailedException(e);
        }

    }

    private String buildConfirmationLink(String token) {
        return "/api/auth/email-confirmation?token=" + token;
    }

}
