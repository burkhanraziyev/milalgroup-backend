package az.milalgroup.service;

import az.milalgroup.dto.request.AuthRequest;
import az.milalgroup.dto.response.AuthenticateResponse;
import az.milalgroup.security.SecurityUtils;
import az.milalgroup.security.jwt.TokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class AuthenticateService {

    private final TokenProvider tokenProvider;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final RefreshTokenService refreshTokenService;
    private final UserService userService;

    public AuthenticateResponse authorize(AuthRequest authRequest) {
        final var authenticationToken = new UsernamePasswordAuthenticationToken(
                authRequest.getUsername(),
                authRequest.getPassword()
        );

        final var authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final var accessToken = tokenProvider.createToken(authentication, authRequest.isRememberMe());
        final var refreshToken = tokenProvider.createToken(authentication,false);
        final var user = userService.findByUsername(SecurityUtils.getCurrentUserLoginOrThrow());
        refreshTokenService.createToken(refreshToken, user);
        return new AuthenticateResponse(accessToken, refreshToken);
    }

}
