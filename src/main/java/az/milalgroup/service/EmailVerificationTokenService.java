package az.milalgroup.service;

import az.milalgroup.domain.EmailVerificationToken;
import az.milalgroup.domain.User;
import az.milalgroup.repository.EmailVerificationTokenRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class EmailVerificationTokenService {

    private final EmailVerificationTokenRepository repository;

    public void create(User user, String token) {
        final var entity = EmailVerificationToken.builder()
                .isAvailable(true)
                .token(token)
                .user(user)
                .expirationDate(LocalDateTime.now().plusHours(1))
                .build();
        repository.save(entity);
    }

}
