package az.milalgroup.service;

import az.milalgroup.config.ApplicationProperties;
import az.milalgroup.domain.RefreshToken;
import az.milalgroup.domain.User;
import az.milalgroup.repository.RefreshTokenRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class RefreshTokenService {

    private final RefreshTokenRepository repository;
    private final ApplicationProperties applicationProperties;

    public void createToken(String token, User user) {
        final var refreshTokenValidity = applicationProperties.getSecurityProperties()
                .getJwtProperties().getRefreshTokenValidity();
        final var refreshToken = RefreshToken.builder()
                .token(token)
                .expirationDate(LocalDateTime.now()
                        .plusDays(refreshTokenValidity))
                .isValid(true)
                .user(user)
                .build();
        repository.save(refreshToken);
    }
}
