package az.milalgroup.exception;

import lombok.Getter;

import static az.milalgroup.constant.ExceptionMessageKeyConstants.ENUM_MISMATCH;

@Getter
public class EnumMismatchException extends RuntimeException {

    private final String messageKey;
    private final Object[] objects;

    public EnumMismatchException(Object[] objects) {
        super(ENUM_MISMATCH);
        this.messageKey = ENUM_MISMATCH;
        this.objects = objects;
    }

}
