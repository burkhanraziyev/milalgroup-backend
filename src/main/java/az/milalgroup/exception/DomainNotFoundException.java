package az.milalgroup.exception;

import lombok.Getter;

@Getter
public class DomainNotFoundException extends RuntimeException {

    private final String messageKey;
    private final Object[] objects;

    public DomainNotFoundException(String messageKey, Object[] objects) {
        super(messageKey);
        this.messageKey = messageKey;
        this.objects = objects;
    }

}
