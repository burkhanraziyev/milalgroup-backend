package az.milalgroup.exception;

import lombok.Getter;

import static az.milalgroup.constant.ExceptionMessageKeyConstants.EMAIL_SEND_FAILED;

@Getter
public class EmailSendFailedException extends RuntimeException {

    private final String messageKey;
    private final String error;

    public EmailSendFailedException(Throwable throwable) {
        super(EMAIL_SEND_FAILED, throwable);
        this.messageKey = EMAIL_SEND_FAILED;
        this.error = throwable.getMessage();
    }

}
