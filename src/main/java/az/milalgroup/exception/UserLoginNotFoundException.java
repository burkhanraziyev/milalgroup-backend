package az.milalgroup.exception;

import lombok.Getter;
import org.springframework.security.core.AuthenticationException;

import static az.milalgroup.constant.ExceptionMessageKeyConstants.USER_LOGIN_NOT_FOUND;

@Getter
public class UserLoginNotFoundException extends AuthenticationException {

    private final String messageKey;

    public UserLoginNotFoundException() {
        super(USER_LOGIN_NOT_FOUND);
        this.messageKey = USER_LOGIN_NOT_FOUND;
    }

}
