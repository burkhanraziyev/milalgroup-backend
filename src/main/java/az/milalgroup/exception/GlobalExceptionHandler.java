package az.milalgroup.exception;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static az.milalgroup.constant.ExceptionMessageKeyConstants.VALIDATION_ERROR;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private final MessageSource messageSource;

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(@NonNull MethodArgumentNotValidException ex,
                                                                  @NonNull HttpHeaders headers,
                                                                  HttpStatusCode status,
                                                                  WebRequest request) {
        Locale locale = LocaleContextHolder.getLocale();
        final var message = messageSource.getMessage(VALIDATION_ERROR, new Object[]{}, locale);
        return buildResponseEntity(HttpStatus.valueOf(status.value()), message,
                getFieldErrors(ex, locale), request.getContextPath());
    }

    @ExceptionHandler(AuthUsernameNotFoundException.class)
    protected ResponseEntity<Object> handle(AuthUsernameNotFoundException ex, WebRequest request) {
        Locale locale = LocaleContextHolder.getLocale();
        final var message = messageSource.getMessage(ex.getMessageKey(), ex.getObjects(), locale);
        return buildResponseEntity(HttpStatus.UNAUTHORIZED, message, request.getContextPath());
    }

    @ExceptionHandler(DomainNotFoundException.class)
    protected ResponseEntity<Object> handle(DomainNotFoundException ex, WebRequest request) {
        Locale locale = LocaleContextHolder.getLocale();
        final var message = messageSource.getMessage(ex.getMessageKey(), ex.getObjects(), locale);
        return buildResponseEntity(HttpStatus.NOT_FOUND, message, request.getContextPath());
    }

    @ExceptionHandler(EmailSendFailedException.class)
    protected ResponseEntity<Object> handle(EmailSendFailedException ex, WebRequest request) {
        Locale locale = LocaleContextHolder.getLocale();
        final var message = messageSource.getMessage(ex.getMessageKey(), new Object[]{}, locale);
        return buildResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR, message, ex.getError(), request.getContextPath());
    }

    @ExceptionHandler(EnumMismatchException.class)
    protected ResponseEntity<Object> handle(EnumMismatchException ex, WebRequest request) {
        Locale locale = LocaleContextHolder.getLocale();
        final var message = messageSource.getMessage(ex.getMessageKey(), ex.getObjects(), locale);
        return buildResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY, message, request.getContextPath());
    }

    @ExceptionHandler(UserBlockedException.class)
    protected ResponseEntity<Object> handle(UserBlockedException ex, WebRequest request) {
        Locale locale = LocaleContextHolder.getLocale();
        final var message = messageSource.getMessage(ex.getMessageKey(), ex.getObjects(), locale);
        return buildResponseEntity(HttpStatus.UNAUTHORIZED, message, request.getContextPath());
    }

    @ExceptionHandler(UserLoginNotFoundException.class)
    protected ResponseEntity<Object> handle(UserLoginNotFoundException ex, WebRequest request) {
        Locale locale = LocaleContextHolder.getLocale();
        final var message = messageSource.getMessage(ex.getMessageKey(), new Object[]{}, locale);
        return buildResponseEntity(HttpStatus.UNAUTHORIZED, message, request.getContextPath());
    }

    @ExceptionHandler(UserNotConfirmedException.class)
    protected ResponseEntity<Object> handle(UserNotConfirmedException ex, WebRequest request) {
        Locale locale = LocaleContextHolder.getLocale();
        final var message = messageSource.getMessage(ex.getMessageKey(), ex.getObjects(), locale);
        return buildResponseEntity(HttpStatus.UNAUTHORIZED, message, request.getContextPath());
    }

    private List<ValidationError> getFieldErrors(MethodArgumentNotValidException ex, Locale locale) {
        return ex
                .getBindingResult()
                .getFieldErrors()
                .stream()
                .map(f ->
                        new ValidationError(
                                f.getField(),
                                f.getObjectName().replaceFirst("DTO$", ""),
                                messageSource
                                        .getMessage(Objects.requireNonNull(f.getDefaultMessage()), f.getArguments(), locale)
                        )
                )
                .toList();
    }

    public ResponseEntity<Object> buildResponseEntity(HttpStatus httpStatus, String message, String path) {
        final var errorResponse = ErrorResponse.builder()
                .reason(httpStatus.getReasonPhrase())
                .code(httpStatus.value())
                .message(message)
                .path(path)
                .timestamp(LocalDateTime.now())
                .build();
        return new ResponseEntity<>(errorResponse, httpStatus);
    }

    public ResponseEntity<Object> buildResponseEntity(HttpStatus httpStatus, String message,
                                                      String error, String path) {
        final var errorResponse = ErrorResponse.builder()
                .reason(httpStatus.getReasonPhrase())
                .code(httpStatus.value())
                .message(message)
                .error(error)
                .path(path)
                .timestamp(LocalDateTime.now())
                .build();
        return new ResponseEntity<>(errorResponse, httpStatus);
    }

    public ResponseEntity<Object> buildResponseEntity(HttpStatus httpStatus, String message,
                                                      List<ValidationError> fieldErrors, String path) {
        final var errorResponse = ErrorResponse.builder()
                .reason(httpStatus.getReasonPhrase())
                .code(httpStatus.value())
                .message(message)
                .fieldErrors(fieldErrors)
                .path(path)
                .timestamp(LocalDateTime.now())
                .build();
        return new ResponseEntity<>(errorResponse, httpStatus);
    }


}
