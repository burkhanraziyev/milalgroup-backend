package az.milalgroup.exception;

import lombok.Getter;

import static az.milalgroup.constant.ExceptionMessageKeyConstants.USER_NOT_CONFIRMED;

@Getter
public class UserNotConfirmedException extends RuntimeException {

    private final String messageKey;
    private final Object[] objects;

    public UserNotConfirmedException(Object[] objects) {
        super(USER_NOT_CONFIRMED);
        this.messageKey = USER_NOT_CONFIRMED;
        this.objects = objects;
    }

}
