package az.milalgroup.exception;

import lombok.Getter;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static az.milalgroup.constant.ExceptionMessageKeyConstants.USER_NOT_FOUND_BY_USERNAME;

@Getter
public class AuthUsernameNotFoundException extends UsernameNotFoundException {

    private final String messageKey;
    private final Object[] objects;

    public AuthUsernameNotFoundException(Object[] objects) {
        super(USER_NOT_FOUND_BY_USERNAME);
        this.messageKey = USER_NOT_FOUND_BY_USERNAME;
        this.objects = objects;
    }

}
