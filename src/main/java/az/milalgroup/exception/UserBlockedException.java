package az.milalgroup.exception;

import lombok.Getter;

import static az.milalgroup.constant.ExceptionMessageKeyConstants.USER_BLOCKED;

@Getter
public class UserBlockedException extends RuntimeException {

    private final String messageKey;
    private final Object[] objects;

    public UserBlockedException(Object[] objects) {
        super(USER_BLOCKED);
        this.messageKey = USER_BLOCKED;
        this.objects = objects;
    }

}
