package az.milalgroup.enumaration.converter;

import az.milalgroup.enumaration.UserStatus;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter(autoApply = true)
public class UserStatusConverter implements AttributeConverter<UserStatus, Integer> {

    @Override
    public Integer convertToDatabaseColumn(UserStatus attribute) {
        // TODO: null check
        return attribute.getId();
    }

    @Override
    public UserStatus convertToEntityAttribute(Integer dbData) {
        // TODO: null check
        return UserStatus.get(dbData);
    }

}
