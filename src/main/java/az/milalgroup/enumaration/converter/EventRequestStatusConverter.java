package az.milalgroup.enumaration.converter;

import az.milalgroup.enumaration.EventRequestStatus;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter(autoApply = true)
public class EventRequestStatusConverter implements AttributeConverter<EventRequestStatus, Integer> {

    @Override
    public Integer convertToDatabaseColumn(EventRequestStatus attribute) {
        // TODO: null check
        return attribute.getId();
    }

    @Override
    public EventRequestStatus convertToEntityAttribute(Integer dbData) {
        // TODO: null check
        return EventRequestStatus.get(dbData);
    }

}
