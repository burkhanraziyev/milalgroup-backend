package az.milalgroup.enumaration.converter;

import az.milalgroup.enumaration.Gender;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter(autoApply = true)
public class GenderConverter implements AttributeConverter<Gender, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Gender attribute) {
        // TODO: null check
        return attribute.getId();
    }

    @Override
    public Gender convertToEntityAttribute(Integer dbData) {
        // TODO: null check
        return Gender.get(dbData);
    }

}
