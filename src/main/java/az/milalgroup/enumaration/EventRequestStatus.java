package az.milalgroup.enumaration;

import az.milalgroup.exception.EnumMismatchException;
import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

@Getter
public enum EventRequestStatus {

    PENDING(1, "Pending"),
    IN_PROGRESS(2, "In Progress"),
    DONE(3, "Done");

    private final Integer id;
    private final String value;

    EventRequestStatus(Integer id, String value) {
        this.id = id;
        this.value = value;
    }

    public static EventRequestStatus get(Integer id) {
        return Arrays.stream(EventRequestStatus.values())
                .filter(eventRequestStatus -> Objects.equals(eventRequestStatus.getId(), id))
                .findFirst()
                .orElseThrow(() -> new EnumMismatchException(new Object[]{id}));
    }

}
