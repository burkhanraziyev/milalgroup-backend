package az.milalgroup.enumaration;

import az.milalgroup.exception.EnumMismatchException;
import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

@Getter
public enum Gender {

    MALE(1, "Male"),
    FEMALE(2, "Female"),
    OTHER(3, "Other");

    private final Integer id;
    private final String value;

    Gender(Integer id, String value) {
        this.id = id;
        this.value = value;
    }

    public static Gender get(Integer id) {
        return Arrays.stream(Gender.values())
                .filter(gender -> Objects.equals(gender.getId(), id))
                .findFirst()
                .orElseThrow(() -> new EnumMismatchException(new Object[]{id}));
    }

}
