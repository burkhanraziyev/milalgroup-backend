package az.milalgroup.enumaration;

import az.milalgroup.exception.EnumMismatchException;
import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

@Getter
public enum FileType {

    PROFILE_IMAGE(1, "Profile Image"),
    EVENT_IMAGE(2, "Event Image"),
    OTHER(3, "Other");

    private final Integer id;
    private final String value;

    FileType(Integer id, String value) {
        this.id = id;
        this.value = value;
    }

    public static FileType get(Integer id) {
        return Arrays.stream(FileType.values())
                .filter(fileType -> Objects.equals(fileType.getId(), id))
                .findFirst()
                .orElseThrow(() -> new EnumMismatchException(new Object[]{id}));
    }
    
}
