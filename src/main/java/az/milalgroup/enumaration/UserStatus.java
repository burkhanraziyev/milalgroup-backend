package az.milalgroup.enumaration;

import az.milalgroup.exception.EnumMismatchException;
import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

@Getter
public enum UserStatus {

    PENDING(1, "Pending"),
    ACTIVE(2, "Active"),
    BLOCKED(3, "Blocked");

    private final Integer id;
    private final String value;

    UserStatus(Integer id, String value) {
        this.id = id;
        this.value = value;
    }

    public static UserStatus get(Integer id) {
        return Arrays.stream(UserStatus.values())
                .filter(userStatus -> Objects.equals(userStatus.getId(), id))
                .findFirst()
                .orElseThrow(() -> new EnumMismatchException(new Object[]{id}));
    }

}
